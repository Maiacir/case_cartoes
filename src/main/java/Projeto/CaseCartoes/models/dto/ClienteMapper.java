package Projeto.CaseCartoes.models.dto;

import Projeto.CaseCartoes.models.Cliente;
import org.springframework.stereotype.Component;

@Component
public class ClienteMapper {

    public Cliente toCliente(SalvarClienteRequest salvarClienteRequest) {
        Cliente cliente = new Cliente();
        cliente.setNome(salvarClienteRequest.getNome());
        return cliente;
    }
}
