package Projeto.CaseCartoes.models.dto;

public class AtualizarCartaoRequest {

    private String numero;

    private Boolean ativo;

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
}
