package Projeto.CaseCartoes.services;

import Projeto.CaseCartoes.models.Cartao;
import Projeto.CaseCartoes.models.Pagamento;
import Projeto.CaseCartoes.repositories.CartaoRepository;
import Projeto.CaseCartoes.repositories.PagamentoRepository;
import net.bytebuddy.implementation.auxiliary.AuxiliaryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    public Pagamento salvarPagamento(Pagamento pagamento) {
        Cartao cartao = cartaoService.buscarPorId(pagamento.getCartao().getId());

        pagamento.setCartao(cartao);

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> buscarTodosCartoes(Long cartaoId) {
       return pagamentoRepository.findAllByCartao_id(cartaoId);
    }
}
