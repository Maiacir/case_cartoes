package Projeto.CaseCartoes.services;

import Projeto.CaseCartoes.exception.CartaoNotFoundException;
import Projeto.CaseCartoes.models.Cartao;
import Projeto.CaseCartoes.models.Cliente;
import Projeto.CaseCartoes.repositories.CartaoRepository;
import Projeto.CaseCartoes.repositories.ClienteRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public Cartao salvarCartao(Cartao cartao) {
       Cliente cliente = clienteService.buscarPorId(cartao.getCliente().getId());

       cartao.setCliente(cliente);
       cartao.setAtivo(false);

       return cartaoRepository.save(cartao);
    }

    public Cartao atualizarCartao(Cartao cartao) {
        Cartao databaseCartao = buscarPorNumero(cartao.getNumero());

        databaseCartao.setAtivo(cartao.getAtivo());

        return cartaoRepository.save(databaseCartao);
    }

    public Cartao buscarPorNumero(String numero) {
        Optional<Cartao> porId = cartaoRepository.findByNumero(numero);

        if (!porId.isPresent()) {
            throw new CartaoNotFoundException();
        }
        return porId.get();
    }

    public Cartao buscarPorId(Long id) {
        Optional<Cartao> porId = cartaoRepository.findById(id);

        if(!porId.isPresent()) {
            throw new CartaoNotFoundException();
        }
        return porId.get();
    }

}