package Projeto.CaseCartoes.services;

import Projeto.CaseCartoes.exception.ClienteNotFoundException;
import Projeto.CaseCartoes.models.Cliente;
import Projeto.CaseCartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente) {
        return clienteRepository.save(cliente);
    }

    public Cliente buscarPorId (Long id) {
        Optional<Cliente> clienteOptional = clienteRepository.findById(id);
        if (!clienteOptional.isPresent()) {
            throw new ClienteNotFoundException();
        }
        return clienteOptional.get();
    }


}
