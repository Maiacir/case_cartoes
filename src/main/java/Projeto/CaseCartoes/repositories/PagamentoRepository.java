package Projeto.CaseCartoes.repositories;

import Projeto.CaseCartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartao_id(Long cartaoId);
}
