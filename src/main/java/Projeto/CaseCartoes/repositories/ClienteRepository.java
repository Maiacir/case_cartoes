package Projeto.CaseCartoes.repositories;

import Projeto.CaseCartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {
}
