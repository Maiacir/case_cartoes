package Projeto.CaseCartoes.controllers;

import Projeto.CaseCartoes.models.Cartao;
import Projeto.CaseCartoes.models.Cliente;
import Projeto.CaseCartoes.models.dto.*;
import Projeto.CaseCartoes.services.CartaoService;
import ch.qos.logback.core.pattern.util.RegularEscapeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.criteria.CriteriaBuilder;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cartoes")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @Autowired
    private CartaoMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public SalvarCartaoResponse salvarCartao(@Valid @RequestBody SalvarCartaoRequest salvarCartaoRequest) {
        Cartao cartao = mapper.toCartao(salvarCartaoRequest);

        cartao = cartaoService.salvarCartao(cartao);

        return mapper.toSalvarCartaoResponse(cartao);
    }

    @PatchMapping("/{numero}")
    public AtualizarCartaoResponse atualizarCartao(@PathVariable String numero, @RequestBody AtualizarCartaoRequest atualizarCartaoRequest) {
        atualizarCartaoRequest.setNumero(numero);
        Cartao cartao = mapper.toCartao(atualizarCartaoRequest);

        cartao = cartaoService.atualizarCartao(cartao);

        return mapper.toAtualizarCartaoResponse(cartao);

    }


    @GetMapping("/{numero}")
    public BuscarCartaoResponse buscarPorNumero(@PathVariable String numero) {
        Cartao cartao = cartaoService.buscarPorNumero(numero);
        return mapper.toBuscarCartaoResponse(cartao);
    }

}
