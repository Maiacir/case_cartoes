package Projeto.CaseCartoes.controllers;

import Projeto.CaseCartoes.models.Cliente;
import Projeto.CaseCartoes.models.dto.ClienteMapper;
import Projeto.CaseCartoes.models.dto.SalvarClienteRequest;
import Projeto.CaseCartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente salvarCliente (@RequestBody SalvarClienteRequest salvarClienteRequest) {
        Cliente cliente = mapper.toCliente(salvarClienteRequest);

        cliente = clienteService.salvarCliente(cliente);

        return cliente;
    }

    @GetMapping("/{id}")
    public Cliente buscarPorId(@PathVariable Long id) {
        return clienteService.buscarPorId(id);
    }

}
