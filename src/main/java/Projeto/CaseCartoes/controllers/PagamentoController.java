package Projeto.CaseCartoes.controllers;

import Projeto.CaseCartoes.models.Pagamento;
import Projeto.CaseCartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    public Pagamento salvarPagamento(@RequestBody Pagamento pagamento) {
        return pagamentoService.salvarPagamento(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoId")
    public List<Pagamento> buscarTodosCartoes(@PathVariable Long cartaoId) {
        return pagamentoService.buscarTodosCartoes(cartaoId);
    }
}
